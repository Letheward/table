/* ==== Basic ==== */

// note: not safe, use with caution
String string_advance(String s, u64 pos) {
    return (String) {s.data + pos, s.count - pos};
}

// note: unsafe, use with caution (also end is exclusive)
String string_view(String s, u64 start, u64 end) {
    return (String) {s.data + start, end - start};
}

String string_eat(String* s, u64 count) {
    if (count > s->count) return (String) {0};
    u8* old = s->data;
    *s = string_advance(*s, count);
    return (String) { old, count };
}

// todo: speed
u8 string_equal(String a, String b) {
    if (a.count != b.count) return 0;
    if (a.data  == b.data ) return 1;
    for (u64 i = 0; i < a.count; i++) {
        if (a.data[i] != b.data[i]) return 0; 
    }
    return 1;
}





/* ---- Basic Utils ---- */

const u8 string_spaces_lookup_table[256] = {
    [' ']  = 1,
    ['\t'] = 1,
    ['\r'] = 1,
    ['\n'] = 1,
};

void string_build_lookup_table(u8 table[256], String s) {
    for (u64 i = 0; i < s.count; i++) {
        table[s.data[i]] = 1;
    }
}




/* ---- String Match ---- */

u8 string_starts_with(String s, String match) {
    if (s.count < match.count) return 0;
    for (u64 i = 0; i < match.count; i++) {
        if (s.data[i] != match.data[i]) return 0;
    }
    return 1;
}

u8 string_ends_with(String s, String match) {
    if (s.count < match.count) return 0;
    u64 pos = s.count - match.count;
    for (u64 i = 0; i < match.count; i++) {
        if (s.data[pos + i] != match.data[i]) return 0;
    }
    return 1;
}

// naive search for now
// todo: validate
// note: we return error by set the .data = NULL, which is not that obvious
String string_find(String a, String b) {
    if (!a.count || !b.count || !a.data || !b.data || (b.count > a.count)) return (String) {0};
    for (u64 i = 0; i < a.count - b.count + 1; i++) {
        for (u64 j = 0; j < b.count; j++) {
            if (a.data[i + j] != b.data[j]) goto next;
        }
        return (String) {a.data + i, a.count - i};
        next: continue;
    }
    return (String) {0};
}




/* ---- Trim & Eat Utils ---- */

u64 string_eat_until(String* s, u8 c) {
    
    u8* p     = s->data;
    u64 count = s->count;
    u64 out   = 0;
   
    for (u64 i = 0; i < count; i++) {
        if (p[i] != c)  out++;
        else            break;
    }
    
    s->data  += out;
    s->count -= out;

    return out;
}
   
u64 string_eat_matches(String* s, const u8 table[256]) {

    u8* p     = s->data;
    u64 count = s->count;
    u64 out   = 0;
   
    for (u64 i = 0; i < count; i++) {
        if (table[p[i]])  out++;
        else              break;
    }
    
    s->data  += out;
    s->count -= out;

    return out;
}

u64 string_eat_not_matches(String* s, const u8 table[256]) {

    u8* p     = s->data;
    u64 count = s->count;
    u64 out   = 0;
   
    for (u64 i = 0; i < count; i++) {
        if (!table[p[i]]) out++;
        else              break;
    }
    
    s->data  += out;
    s->count -= out;

    return out;
}





/* ---- Lazy Allocated Splitting ---- */

String string_eat_by_u8_separator(String* s, u8 c) {
    
    u8* data  = s->data;
    u64 count = string_eat_until(s, c);
   
    if (s->count) {
        s->data++;
        s->count--;
    }
    
    if (!count) return (String) {0};
    
    return (String) { data, count };
}

String string_eat_by_any_u8_matches_using_table(String* s, const u8 table[256]) {
   
    string_eat_not_matches(s, table);
    
    u8* data  = s->data;
    u64 count = string_eat_matches(s, table);
    
    string_eat_not_matches(s, table);
    
    return (String) { data, count };
}

String string_eat_by_any_u8_separators_using_table(String* s, const u8 table[256]) {

    string_eat_matches(s, table);
    
    u8* data  = s->data;
    u64 count = string_eat_not_matches(s, table);
    
    string_eat_matches(s, table);
    
    return (String) { data, count };
}

// note: this is slower than using_table version, but easier to use
String string_eat_by_any_u8_matches(String* s, String match) {
    u8 table[256] = {0};
    string_build_lookup_table(table, match);
    return string_eat_by_any_u8_matches_using_table(s, table);
}

// note: this is slower than using_table version, but easier to use
String string_eat_by_any_u8_separators(String* s, String separators) {
    u8 table[256] = {0};
    string_build_lookup_table(table, separators);
    return string_eat_by_any_u8_separators_using_table(s, table);
}

String string_eat_by_spaces(String* s) {
    return string_eat_by_any_u8_separators_using_table(s, string_spaces_lookup_table);
}

String string_eat_line(String* s) {
    String line = string_eat_by_u8_separator(s, '\n');
    if (!line.count) return line;
    if (line.data[line.count - 1] == '\r') line.count--;
    return line;
}






// basic print
void print_string(String s) {
    fwrite(s.data, sizeof(u8), s.count, stdout);
}

u64 file_print(FILE* f, String s) {
    fwrite(s.data, sizeof(u8), s.count, f);
    return s.count;
}

char* temp_c_string(String s) {
    char* out = temp_alloc(s.count + 1);
    memcpy(out, s.data, s.count);
    out[s.count] = 0;
    return out;
}

// todo: handle alloc failed
String string_copy(String s) {
    String out = { malloc(s.count), s.count };
    for (u64 i = 0; i < s.count; i++) out.data[i] = s.data[i]; // todo: speed
    return out;
}


// todo: do we really need to switch allocator?
String load_file(char* path) {

    FILE* f = fopen(path, "rb");
    if (!f) return (String) {0}; // todo: this is not enough, what if we have a file of size 0? 

    fseek(f, 0, SEEK_END);
    u64 count = ftell(f);
    fseek(f, 0, SEEK_SET);

    u8* data = malloc(count);
    if (!data) {
        fclose(f);
        return (String) {0}; 
    }
    
    fread(data, 1, count, f);
    fclose(f);

    return (String) {data, count};
}


// todo: not robust, need more testing, handle adjacent items (no space in between)
void print(String s, ...) {
    
    va_list args;
    va_start(args, s);
    
    for (u64 i = 0; i < s.count; i++) {

        u8 c = s.data[i];
        if (c == '@') {
            if (i + 1 < s.count && s.data[i + 1] == '@') { // short circuit 
                putchar('@');
                i++;
            } else {
                print_string(va_arg(args, String)); // not safe, but this is C varargs, what can you do 
            }
            continue;
        }

        putchar(c);
    }
    
    va_end(args);
}


// note: does not give a copy
String read_line() {
    
    String s = context.input_buffer;

    fgets((char*) s.data, s.count, stdin);
    s.count = strlen((const char*) s.data);
    
    if (s.count == 0) return (String) {0};
    if (s.data[s.count - 1] == '\n') s.count -= 1;
    
    return s;
}









/* ==== Number Parsing ==== */

// todo: validate
u8 parse_u64(String s, u64* out) {

    if (!s.count) return 0;

    u64 start  = 0;
    while (start < s.count && s.data[start] <= ' ') start++;
    
    u64 result = 0;
    u64 length = 0;
    
    for (u64 i = start; i < s.count; i++) {
        
        u8 c = s.data[i];
        if (c < '0' || c > '9') return 0;
        
        result *= 10;
        result += c - '0';

        length++;
    }

    if (length == 0 || length > 20) return 0; // todo: not handling all overflows

    *out = result;

    return 1;
}

// todo: validate
u8 parse_s64(String s, s64* out) {

    if (!s.count) return 0;

    u64 start  = 0;
    while (start < s.count && s.data[start] <= ' ') start++;
 
    s64 sign   = 1;
    switch (s.data[start]) {
        case '-': sign = -1; // fall-through
        case '+': start++;
        default:  break;
    }
   
    s64 result = 0;
    u64 length = 0;
    
    for (u64 i = start; i < s.count; i++) {
        
        u8 c = s.data[i];
        if (c < '0' || c > '9') return 0;
        
        result *= 10;
        result += c - '0';

        length++;
    }

    if (length == 0 || length > 20) return 0; // todo: not handling all overflows

    *out = sign * result;

    return 1;
}

// todo: validate
// todo: precision
u8 parse_f64(String s, f64* out) {
    
    if (!s.count) return 0;

    u64 start  = 0;
    while (start < s.count && s.data[start] <= ' ') start++;
 
    f64 sign   = 1;
    switch (s.data[start]) {
        case '-': sign = -1; // fall-through
        case '+': start++;
        default:  break;
    }
   
    f64 result = 0;
    
    u64 dot_pos = start;
    while (dot_pos < s.count) {
       
        u8 c = s.data[dot_pos];
        
        if (c == '.') {
            break;
        } else {
            if (c < '0' || c > '9') return 0;
        }

        dot_pos++;
    }
    
    for (u64 i = start; i < dot_pos; i++) {
        result *= 10.0;
        result += s.data[i] - '0';
    }

    f64 denom = 10;
    for (u64 i = dot_pos + 1; i < s.count; i++) {
        
        u8 c = s.data[i];
       
        if (c < '0' || c > '9') return 0;
        
        result += (f64) (c - '0') / denom;
        denom *= 10;
    }
    
    *out = sign * result;

    return 1;
}


